package com.lab10.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5,3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2,2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle cir1 = new Circle(2);
        System.out.println(cir1);
        System.out.printf("%s area: %.3f \n ",cir1.getName(),cir1.calArea());
        System.out.printf("%s perimeter: %.3f \n ",cir1.getName(),cir1.calPerimeter());

        Circle cir2 = new Circle(3);
        System.out.println(cir2);
        System.out.println(cir2.calArea());
        System.out.println(cir2.calPerimeter());
         
        Triangle tri1 = new Triangle(2, 2, 2);
        System.out.println(tri1);
        System.out.println(tri1.calArea());
        System.out.println(tri1.calPerimeter());

        Triangle tri2 = new Triangle(3, 3, 3);
        System.out.println(tri2);
        System.out.println(tri2.calArea());
        System.out.println(tri2.calPerimeter());

    }
    
}
